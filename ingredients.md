Ingredients
===========

* Olive Oil
* Chicken Breasts, Diced (2)
* Butternut Squash, peeled (1)
* Red Onion (1)
* Ginger (1 pinch)
* Peas (1/3 cup)
* Canned Tomatoes(1 cup)
* Coconut Milk (1 cup)
* Curry Powder (2 1/2 teaspoons)
* Chicken or Vegetable Stock
* Chili, Chopped (1)
* Spinach (1/2 cup)
