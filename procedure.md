Procedure
=========

1. Put your non-stick **pan** over medium high heat.
2. Add oil once warm.
3. Meanwhile, using the large side of **cheese grater**, grate the squash and add to pain. Season with salt and add curry powder.
4. Grate the onion as the squash cooks and add to mixture.
5. Let cook for 1-2 minutes then add chili.
6. After a minute of cooking, use a **microplane** and shave the ginger into the mixture.
7. Once mixture is nicely cooked, add chicken and toss.
8. After about 2 minutes, add peas.
9. After a minute add tomatoes and after another minute the coconut milk.
10. Mix and cook for about 2 minutes.
11. Then add 2 tablespoons of stock bring to boil then simmer until chicken is cooked, around 5 minutes.
